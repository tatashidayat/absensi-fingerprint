//  LCD
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

// Fingerprint
#include <Adafruit_Fingerprint.h>
SoftwareSerial mySerial(D7, D4);
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);
int maxJari = 0;

// WiFi
#include <ESP8266WiFi.h>
const char* ssid     = "@th.id";
const char* password = "mujidwi223";
const char* host = "smkbundakandung.000webhostapp.com";
const char* token = "1fdc0f893412ce55f0d2811821b84d3b";
WiFiClient client;

// Konstanta
#define ID_TIDAK_DIKENAL  0
#define ID_ADMIN          1
String NAMA_SMK = "  SMK BUNDA KANDUNG ";
String nama_siswa = " ";
boolean registered = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Setup");
  lcdInit();
  fingerInit();
  wifiInit();
  delay(1000);
}

void lcdInit() {
  lcd.init();
  lcd.backlight();
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "Inisialisasi");
}

void fingerInit() {
  finger.begin(57600);
  if (finger.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
    tampilLCD(2, "Sensor Ditemukan");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    tampilLCD(2, "Sensor ERROR!");
    while (1) {
      delay(1000);
    }
  }
  maxJari = getNewID();
  Serial.print("Sensor contains "); Serial.print(maxJari); Serial.println(" templates");
  Serial.println("Waiting for valid finger...");
}

void wifiInit() {
  tampilLCD(2, "  Menghubungkan ke  " );
  tampilLCD(3,String(ssid));
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) delay(100);
  tampilLCD(2, "    Terhubung ke    ");
}

void loop() {
  // put your main code here, to run repeatedly:
  int idJari = -1;
  Serial.println("Siap-siap");
  tampilHome();
  while (idJari < 0) {
    idJari = getIDJari();
  }
  if (idJari == ID_TIDAK_DIKENAL) {
    Serial.println("Jari TIdak Dikenal");
    tampilAbsenGagal();
    delay(2000);
    return;
  }
  if (idJari == ID_ADMIN) {
    Serial.println("Jari Admin");
    tampilLCD(2, "  -- ADMIN MODE --  ");
    delay(2000);
    int id = tambahJari();
    if(registered) return;
    tampilLCD(2, "Perekaman Berhasil");
    tampilLCD(3, "Mengirim data jari");
    boolean pass = kirimID(id);
    while (!pass) {
      pass = kirimID(id);
      delay(250000);
    }
    tampilLCD(3, "ID Jari Anda: " + String(id));
    delay(1000);
    return;
  }
  tampilJariKenal();
  if (cekID(idJari)) tampilAbsenBerhasil(nama_siswa);
  else tampilJariBelumDaftar(idJari);
  delay(2000);
}

void tampilHome() {
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "    Absensi Siswa   ");
  tampilLCD(2, " ");
  tampilLCD(3, " ");
}

void tampilAbsenGagal() {
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "    Absensi Siswa   ");
  tampilLCD(2, "Absensi Gagal");
  tampilLCD(3, "Jari Tidak Terdaftar");
}

void tampilAbsenBerhasil(String nama) {
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "    Absensi Siswa   ");
  tampilLCD(2, "Hi, " + nama);
  tampilLCD(3, "Absensi Berhasil");
}

void tampilJariKenal() {
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "    Absensi Siswa   ");
  tampilLCD(2, "Mengirim Data Jari");
  tampilLCD(3, " ");
}

void tampilJariBelumDaftar(int id) {
  tampilLCD(0, NAMA_SMK);
  tampilLCD(1, "    Absensi Siswa   ");
  tampilLCD(2, "Absensi Gagal");
  if (nama_siswa==" ") tampilLCD(3, "Kesalahan koneksi!");
  else tampilLCD(3, "Daftarkan ID:" + String(id));
}

void tampilLCD(int baris, String pesan) {
  lcd.setCursor(0, baris);
  lcd.print(pesan);
  if (pesan.length() < 20) {
    for (int i = 0; i < 20 - pesan.length(); i++) lcd.print(" ");
  }
}


int getIDJari() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return 0;

  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID);
  Serial.print(" with confidence of "); Serial.println(finger.confidence);
  return finger.fingerID;
}



int tambahJari() {
  //  tampilLCD(0, "SMK APA INI");
  tampilLCD(3, "Masukkan Jari Anda");
  Serial.println("Tambah Jari");
  int idCek = -1;
  while (idCek == -1) {
    idCek = getIDJari();
  }
  if (idCek != 0) {
    registered=true;
    return idCek;
  }
  registered=false;

  int id = getNewID();
  int p = -1;
  Serial.print("Waiting for valid finger to enroll as #"); Serial.println(id);
  tampilLCD(3, "PROSES PEREKAMAN");
  p = finger.getImage();
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
  }

  // OK success!
  p = finger.image2Tz(1);
  while (p != FINGERPRINT_OK) {
    p = finger.image2Tz(1);
  }

  Serial.println("Remove finger");
  tampilLCD(3, "Lepaskan Jari Anda");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(id);
  p = -1;

  Serial.println("Place same finger again");
  tampilLCD(3, "Masukan Jari Anda");
  p = finger.getImage();
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
  }

  // OK success!
  p = finger.image2Tz(2);
  while (p != FINGERPRINT_OK) {
    p = finger.image2Tz(2);
  }

  // OK converted!
  Serial.print("Creating model for #");  Serial.println(id);
  p = finger.createModel();
  while (p != FINGERPRINT_OK) {
    p = finger.createModel();
  }

  Serial.print("ID "); Serial.println(id);
  p = finger.storeModel(id);
  while (p != FINGERPRINT_OK) {
    p = finger.storeModel(id);
  }
  return id;
}

int getNewID() {
  finger.getTemplateCount();
  maxJari = finger.templateCount;
  return maxJari + 1;
  // belum selesai
}


boolean cekID(int id) {
  nama_siswa = " ";
  String url = "/api/absen.php?id=" + String(id);

  // ini
  Serial.print("connecting to ");
  Serial.println(host);
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return false;
  }

  // We now create a URI for the request
  Serial.print("Requesting URL: ");
  Serial.println(url);
  String header = String("GET ") + url + " HTTP/1.1\r\n" +
                  "Host: " + host + "\r\n" +
                  "Token: " + token + "\r\n" +
                  "Connection: close\r\n\r\n";
  Serial.println(header);

  // This will send the request to the server
  client.print(header);

  unsigned long timeout = millis();
  Serial.println();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return false;
    }
  }
  Serial.println();

  // Read all the lines of the reply from server and print them to Serial
  char *buffData = "</p>";
  String data;
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
    if (strstr(line.c_str(), buffData) != NULL) {
      data = line;
      data = data.substring(0, data.length() - 4);
      data.trim();
      nama_siswa = data;
      if (data.length() > 2) return true;
      else return false;
    }
  }
  Serial.println();
  Serial.println("closing connection");
  return false;
}

boolean kirimID(int id){
  String url = "/api/finger.php?id=" + String(id);

  // ini
  Serial.print("connecting to ");
  Serial.println(host);
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return false;
  }

  // We now create a URI for the request
  Serial.print("Requesting URL: ");
  Serial.println(url);
  String header = String("GET ") + url + " HTTP/1.1\r\n" +
                  "Host: " + host + "\r\n" +
                  "Token: " + token + "\r\n" +
                  "Connection: close\r\n\r\n";
  Serial.println(header);

  // This will send the request to the server
  client.print(header);

  unsigned long timeout = millis();
  Serial.println();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return false;
    }
  }
  Serial.println();

  // Read all the lines of the reply from server and print them to Serial
  char *buffData = "</p>";
  String data;
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
    if (strstr(line.c_str(), buffData) != NULL) {
      data = line;
      data = data.substring(0, data.length() - 4);
      data.trim();
      if (data=="OK") return true;
      else return false;
    }
  }
  Serial.println();
  Serial.println("closing connection");
  return false;
}
